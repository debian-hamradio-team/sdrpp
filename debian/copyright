Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SDRPlusPlus
Source: https://github.com/AlexandreRouma/SDRPlusPlus/

Files: *
License: GPL-3

Files: debian/*
Copyright: 2020 Christoph Berg <myon@debian.org>
           2021 Kyle Robbertze <paddatrapper@debian.org>
License: GPL-3

Files: core/src/json.hpp
Copyright: 2013-2019 Niels Lohmann <http://nlohmann.me>
License: Expat

Files: discord_integration/discord-rpc/*
Copyright: 2017 Discord, Inc.
License: Expat

Files: discord_integration/discord-rpc/include/rapidjson/*
Copyright: 2015 THL A29 Limited, a Tencent company, and Milo Yip
License: Expat

Files: core/src/gui/files_dialogs.h
Copyright: 2018-2020 Samp Hocevar <sam@hocevar.net>
License: GPL-3

Files: core/src/imgui/*
Copyright: 2017 Sean Barrett
License: public-domain

Files: core/src/spdlog/*
Copyright: 2015-present Gabi Melman & spdlog contributors
License: Expat

Files: sddc_source/src/libsddc/*
Copyright: 2017-2020 Oscar Steila ik1xpc<at>gmail.com
License: Expat

Files: spyserver_source/src/spyserver_protocol.h
Copyright: 2017 Youssef Touil youssef@live.com
License: GPL-3

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3 as
 published by the Free Software Foundation
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-3".
